﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using Xamarin.Forms;

namespace ShareRecipe
{
    public partial class MorePlayerInfo : ContentPage
    {
        string link = "www.futhead.com";
        public MorePlayerInfo()
        {
            InitializeComponent();
        }
        public MorePlayerInfo(templateMaker player)
        {
            InitializeComponent();
            BindingContext = player;
            link = player.url;
        }
        void Handle_Clicked(object sender, System.EventArgs e)
        {
            var uri = new Uri(link);
            Device.OpenUri(uri);
        }
    }
}
